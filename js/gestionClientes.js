
var clientesObtenidos;

function getClientes(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function (){
    if (this.readyState == 4 && this.status == 200) {
    //  console.log(request.responseText);
    clientesObtenidos = request.responseText;
    procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarClientes() {
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divClientes = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");


  for (var i = 0; i < JSONClientes.value.length; i++) {
  //  console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombreContacto = document.createElement("td");
    columnaNombreContacto.innerText = JSONClientes.value[i].ContactName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;
    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if (JSONClientes.value[i].Country == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    } else { +".png";
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country +".png";
    }
    //columnaBandera.innerText = JSONClientes.value[i].Country;
    columnaBandera.appendChild(imgBandera);
    //<img src="https://www.countries-ofthe-world.com/flags-normal/?$filter=flag-of-Macedonia.png" alt="">

    nuevaFila.appendChild(columnaNombreContacto);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila)
  }
  tabla.appendChild(tbody);
  divClientes.appendChild(tabla);
}
